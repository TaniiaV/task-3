const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user
    getUsers() {
        const items =  UserRepository.getAll();
        if(!items) {
            return null;
        }
        return items;
    }

    createUser(data) {
        const item = UserRepository.create(data);
        console.log(item);
        if(!item){
            return null;
        }
        return item;
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    updatedUser(id, dataToUpdate) {
        const item = UserRepository.update(id, dataToUpdate);
        console.log(item);
        if(!item){
            return null;
        }
        return item;
    }

    deleteUser(id) {
        const item = UserRepository.delete(id);
        console.log(item);
        if(!item){
            return null;
        }
        return item;
    }
}

module.exports = new UserService();