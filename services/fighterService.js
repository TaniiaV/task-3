const { FighterRepository } = require('../repositories/fighterRepository');
const { fighter } = require('../models/fighter');

class FighterService {
    // TODO: Implement methods to work with fighters
    getFighters() {
        const items = FighterRepository.getAll();
        if(!items) {
            return null;
        }
        return items;
    }

    getOneFighter(search) {
        const item = FighterRepository.getOne(search);
        console.log(item)
        if(!item) {
            return null;
        }
        return item;
    }

    createFighter(data) {
        data.health = fighter.health;
        const item = FighterRepository.create(data)
        console.log(item);
        if(!item){
            return null;
        }
        return item;
    }

    updatedFighter(id, dataToUpdate) {
        const item = FighterRepository.update(id, dataToUpdate);
        console.log(item);
        if(!item){
            return null;
        }
        return item;
    }

    deleteFighter(id) {
        const item = FighterRepository.delete(id);
        console.log(item);
        if(!item){
            return null;
        }
        return item;
    }


}

module.exports = new FighterService();