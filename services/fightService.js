const { FightRepository } = require('../repositories/fightRepository');

class FightersService {
    // OPTIONAL TODO: Implement methods to work with fights
    startFight() {
        const id = FightRepository.generateId();
        console.log(id);
        if(!id) {
            return null;
        }
        return id;
    }
}

module.exports = new FightersService();