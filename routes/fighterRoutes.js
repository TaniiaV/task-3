const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid, createFighterFieldsValid } = require('../middlewares/fighter.validation.middleware');
const { check, body, oneOf, validationResult } = require('express-validator');

const router = Router();

router.get('/', (req, res, next) => {
    let fighters = FighterService.getFighters();
    try {
        if(fighters) {
            res.send(fighters);
        }
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
});

router.get('/:id', (req, res, next) => {
    let fighter = FighterService.getOneFighter(req.body);
    try {
        if(fighter) {
            res.send(fighter);
        } else {
            return res.status(400).json({
                error: true,
                message: 'Fighter not exist'
              });
        }

    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
});

router.post('/', createFighterFieldsValid(), createFighterValid, (req, res, next) => {

    try {
        let fighterKeys = Object.keys(req.body);
        if (req.body.id || fighterKeys.length > 3) {
            return res.status(400).json({
                error: true,
                message: 'Not correct fields'
                });
        }

        let getAllFighters = FighterService.getFighters();
        const existFighter = getAllFighters.find(item => item.name === req.body.name);

        if (existFighter) {
            return res.status(400).json({
                error: true,
                message: 'Fighter exist'
              });
        } else {
            const newFighter = FighterService.createFighter(req.body);
            res.send(newFighter);
        }

    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
});

router.put('/:id', createFighterFieldsValid(), updateFighterValid, (req, res, next) => {
    try {
        let userKeys = Object.keys(req.body);
        if (req.body.id || userKeys.length > 3) {
            return res.status(400).json({
                error: true,
                message: 'Not correct fields'
                });
        }

        let getAllFighters = FighterService.getUsers();
        const existFighter = getAllFighters.find(item => item.id === req.params.id);
        
        if (existFighter) {
            const updateFighter = FighterService.updatedFighter(req.params.id, req.body);
            res.send(updateFighter);
        } else {
            return res.status(400).json({
                error: true,
                message: 'Fighter not exist'
              });
        }

    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
});

router.delete('/:id', (req, res, next) => {
    try {
        let getAllFighters = FighterService.getFighters();
        const existFighter = getAllFighters .find(item => item.id === req.params.id);

        if(existFighter) {
            let deletedFighter = FighterService.deleteFighter(req.params.id);
            return res.status(200).json({
                message: 'Fighter deleted'
              });
        } else {
            return res.status(404).json({
                error: true,
                message: 'Fighter not exist'
              });
        }
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
});

module.exports = router;