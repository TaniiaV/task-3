const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid, createUserFieldsValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');
const {check, body, oneOf, validationResult} = require('express-validator');
const { user } = require('../models/user');



const router = Router();

// TODO: Implement route controllers for user
router.get('/', (req, res, next) => {
    let users = UserService.getUsers();
    try {
        if(users) {
            res.send(users);
        }
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
});

router.get('/:id', (req, res, next) => {
    let user = UserService.search(req.body);
    console.log(user)
    try {
        if(user) {
            res.send(user);
        } else {
            return res.status(400).json({
                error: true,
                message: 'User not exist'
              });
        }
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
});

router.post('/', createUserFieldsValid(), createUserValid, (req, res, next) => {

    try {
        let userKeys = Object.keys(req.body);
        if (req.body.id || userKeys.length > 5) {
            return res.status(400).json({
                error: true,
                message: 'Not correct fields'
                });
        }
               
        let getAllUsers = UserService.getUsers();
        const existUser = getAllUsers.find(item => item.email === req.body.email || item.phoneNumber === req.body.phoneNumber);

        if (existUser) {
            return res.status(400).json({
                error: true,
                message: 'User exist'
              });
        } else {
            const newUser = UserService.createUser(req.body);
            res.send(newUser);
        }
        
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
});

router.put('/:id', createUserFieldsValid(), updateUserValid, (req, res, next) => {

    try {
        let userKeys = Object.keys(req.body);
        if (req.body.id || userKeys.length > 5) {
            return res.status(400).json({
                error: true,
                message: 'Not correct fields'
                });
        }

        let getAllUsers = UserService.getUsers();
        const existUser = getAllUsers.find(item => item.id === req.params.id);
        
        if (existUser) {
            const updateUser = UserService.updatedUser(req.params.id, req.body);
            res.send(updateUser);
        } else {
            return res.status(400).json({
                error: true,
                message: 'User not exist'
              });
        }

    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
});

router.delete('/:id', (req, res, next) => {

    try {
        let getAllUsers = UserService.getUsers();
        const existUser = getAllUsers.find(item => item.id === req.params.id);

        if(existUser) {
            let deletedUser = UserService.deleteUser(req.params.id);
            return res.status(200).json({
                message: 'User deleted'
              });
        } else {
            return res.status(404).json({
                error: true,
                message: 'User not exist'
              });
        }
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
});

module.exports = router;