const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { check } = require('express-validator');

const router = Router();

router.post('/login', check('email').isEmail().withMessage('Enter a valid email address'),
    check('password').not().isEmpty().isLength({min: 3}).withMessage('Password is required and must be at least 3 chars long'),
    responseMiddleware, (req, res, next) => {

    try {
        // TODO: Implement login action (get the user if it exist with entered credentials)
        const user = AuthService.login(req.body);
        if(user) {
            res.send(user);
        } 
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
});

module.exports = router;