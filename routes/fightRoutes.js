const { Router } = require('express');
const FightService = require('../services/fightService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');


const router = Router();

// OPTIONAL TODO: Implement route controller for fights
router.post('/', (req, res, next) => {
    console.log('router');
    let fight = FightService.startFight(req.body);
    console.log(fight);

    try {
        if(fight) {
            res.send(fight);
        }
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
});

module.exports = router;