const { fighter } = require('../models/fighter');
const { check, validationResult } = require('express-validator');

const createFighterFieldsValid = () => {
  return [
          check('name').not().isEmpty().isLength({min: 2}).withMessage('Enter a valid name'),
          check('power').not().isEmpty().isNumeric().custom(val => {
              if(val <= 100 && val >= 1) {
                  return true;
              } 
              return false;
          }),    
          check('defense').not().isEmpty().isNumeric().custom(val => {
              if(val <= 10 && val >= 1) {
                  return true;
              } 
              return false;
         })
        ]
}
const createFighterValid = (req, res, next) => {

    const errors = validationResult(req)
    if (errors.isEmpty()) {
      return next();
    }
  
    return res.status(400).json({
      error: true,
      message: 'Fighter data is not valid'
    });
      
}

const updateFighterValid = (req, res, next) => {
  const errors = validationResult(req);
    
  if (errors.isEmpty()) {
    return next();
  }

  return res.status(400).json({
    error: true,
    message: 'Fighter data is not valid'
  });
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
exports.createFighterFieldsValid = createFighterFieldsValid;