const { user } = require('../models/user');
const { check, validationResult } = require('express-validator');

const createUserFieldsValid = () => {
  return [
    check('email').isEmail().custom(val => {
        const reg = /[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-](@gmail\.com)$/;
        if (reg.test(val)) {
            return true;
        }
        return false;
    }).withMessage('Enter a valid email address'),
    check('password').not().isEmpty().isLength({min: 3}).withMessage('Password is required and must be at least 3 chars long'),
    check('firstName').not().isEmpty().isLength({min: 2}).withMessage('First name is required and should had minimum 2 symbols'),
    check('lastName').not().isEmpty().isLength({min: 2}).withMessage('Last name is required and should had minimum 2 symbols'),
    check('phoneNumber').not().isEmpty().custom(val => {
        const reg = /\+380\d{9}/;
        if (reg.test(val)) {
            return true;
        }

        return false;
    })
  ]
}

const createUserValid = (req, res, next) => {
    const errors = validationResult(req);
    
    if (errors.isEmpty()) {
      return next();
    }

    return res.status(400).json({
      error: true,
      message: 'User data is not valid'
    }); 
}

const updateUserValid = (req, res, next) => {

    const errors = validationResult(req);
    
    if (errors.isEmpty()) {
      return next();
    }

    return res.status(400).json({
      error: true,
      message: 'User data is not valid'
    });
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
exports.createUserFieldsValid = createUserFieldsValid;