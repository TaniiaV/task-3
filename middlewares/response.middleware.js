const { check, validationResult } = require('express-validator');
const UserService = require('../services/userService');


const responseMiddleware = (req, res, next) => {
   let getAllUsers = UserService.getUsers();
    const existUser = getAllUsers.find(item => item.email === req.body.email);

    if (existUser) {
        return next();
    } else {
        return res.status(404).json({
            error: true,
            message: 'User not found'
        }); 
    }
    
}

exports.responseMiddleware = responseMiddleware;